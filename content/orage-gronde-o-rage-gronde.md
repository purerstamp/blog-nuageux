---
Title: "Orage gronde, ô rage gronde"
Description: Un phénomène météorologique qui fascine autant qu'il effraie, autrefois symbole de l'ire divine
Date: 26 February 2022
Author: purerstamp
Profile: https://cloud.the-redunicorn.fr/u/edouard
Img: "%assets_url%/foudre.jpg"
Template: post
---  

Mon premier souvenir d'orage remonte à mes premières années. Je pense qu'il s'agit de mon plus lointain souvenir. Quelques images éparses. Une lumière blanche, vive, intermittante. De l'eau. Étonnament, de l'eau. Je l'apprendrai bien plus tard, il s'agissait d'une goutière au plafond. Et enfin, un sentiment d'insécurité.

Ce sentiment m'a longtemps suivi, très longtemps, à chaque orage. Cela fait quelques années seulement que je suis passé de la peur à la fascination. Mais je dois avouer que, quand il n'était pas au dessus de moi et menaçant de sa voix de baryton, l'orage me fascinait. J'avais effectivement une passion pour la météorologie et quel phénomène plus emblématique et mystérieux que celui-ci dans cette discipline ? L'orage est insaisissable, sa physique est complexe, difficile à prévoir. L'éclair quant à lui, zigzague à la vitesse de la lumière et se montre bien avant de gronder, fendant l'air dans un déchirement sourd et brutal. Une rage qui s'abat dans le ciel, sur Terre, l'ire des dieux disait-on à une autre époque.

Désormais, la passion l'emporte. Quelques minutes d'un orage en Amazonie ont été capturées par le micro de Félix Blume. « Fermez les yeux ». Grondement sourd, lointain. Légère pluie sur la canopée. Grondement de plus en plus distinct, les notes se détachent. La canopée ne peut plus retenir l'eau, la pluie s'intensifie, les déchirements dans le ciel sont clairs, nets, de grosses gouttes sur les objets alentours se distinguent, tout s'accélère, tout s'amplifie, tout s'intensifie.. *(expire)* quelle beauté ! Quiconque s'est retrouvé en extérieur lors du passage d'un orage reconnaîtra sans doute ce mouvement dont les thèmes sont presque toujours les mêmes, dans le même ordre. Une vraie symphonie qui nous transporte, avec prudence tout de même, la tête dans les nuages. 

---

Ressources

- Image d'en-tête : Par Hansueli Krapf — Travail personnel Simisa (d · contributions), CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=7336941
- La captation citée, d'un orage amazonien, par Félix Blume : https://www.arteradio.com/son/61662149/amazonia_6_10_orage