---
Title: "Plaisir d'écoute #2"
Description: "Allegro con fuoco pour les 80 ans du pape Benoit XVI"
Date: 25 February 2022
Author: purerstamp
Profile: https://cloud.the-redunicorn.fr/u/edouard
Img: "%assets_url%/dvorak-dudamel.png"
Template: post
--- 

Quel autre chef d'orchestre que Gustavo Dudamel pour faire apprécier dans sa force le 4è mouvement de la 9è symphonie de Dvořák ?

Nulle prétention d'expert ou de critique dans le domaine musical de ma part, en particulier en symphonique. En revanche, je sais quand une oeuvre, un morceau me plaît et j'aime m'en imprégner. Et dans le style, ce mouvement "prend aux tripes" il faut dire. La puissance de son thème principal, décliné entre cuivres et cordes nous transporte et nous transcende *allegro con fuoco*.

Lorsque le chef d'orchestre Gustavo Dudamel dirige avec passion et intensité ce mouvement devant le pape Benoit XVI qui fête ses 80 ans, on ne peut que ressentir plus intensément cette puissance. Dudamel a le désir de rendre la musique symphonique accessible, ce qui se ressent jusque dans sa façon de conduire l'orchestre. Il suffit de porter l'attention sur sa gestuelle et son expressivité tout en écoutant le jeu des instruments pour comprendre la musique. Comprendre la musique. Saisir les émotions qu'elle instancie.

C'est grâce à cette représentation que j'ai découvert ce mouvement dans toute sa grandeur. Final d'une symphonie dite "du nouveau monde", on gagne en hauteur jusqu'à avoir.. la tête dans les nuages !

---
Ressources

- Captation de la représentation citée : https://www.youtube.com/watch?v=jVDofBFtvwA