---
Title: "Plaisir d'écoute #3"
Description: "Fool's Overture, envolée lyrique dans la stratosphère d'un rock très progressif"
Date: 01 March 2022
Author: purerstamp
Profile: https://cloud.the-redunicorn.fr/u/edouard
Img: "%assets_url%/fools-overture.jpg"
Template: post
--- 

Dès les premières mesures, on imagine un musicien jouant sur ce piano à queue enneigé qui constitue la couverture de l'album. Cet album c'est *Even In The Quietest Moments*. Ce morceau c'est *Fool's Overture*. En voici une interprétation alternative et imagée.

La mélodie de départ est douce, chaque note y résonne clairement comme l'écho de la montagne. Un son difficile à définir s'ajoute, un peu hors du temps, on ressent le vent qui souffle sur ce sommet. L'air est frais, le silence enveloppe chaque note du piano. Un crescendo marqué par les voix provenant du fond de la vallée rompt cette harmonie. Les cuivres lancent l'assaut, il est l'heure. Et quand le temps nous rattrape, c'est la réalité qui s'impose à nous.

Tout devient plus rock, plus entraînant. Mais rapidement, le saxophone nous emporte dans sa mélodie lascive, des paroles nous portent et nous transportent.

*« The island's sinking, let's take to the skyyy »*

Suspendus à ces paroles, on s'élève encore avec le saxophone de John Helliwell et... c'est la chute libre. Un retour à la réalité ? On entend les paroles *« Dreamer... »*, cela ne dure pas. Une confusion entre cette scène hors du temps de la pochette, perchée sur un sommet dont on entend encore le souffle du vent et une tout autre histoire, plus réelle. Et c'est ainsi que l'on s'échappe dans un ultime mouvement, entraînant, rapide mais fuyant. Une fuite en cacophonie orchestrale, en decrescendo.

Ce morceau est progressif dans son écriture et dans son jeu. Mais il y a ce quelque chose d'inhabituel qui provoque une confusion dans la visualisation de l'oeuvre. Ne serait-ce pas l'empreinte du rock symphonique ? Une alternance de plus de dix minutes entre mélodies et crescendo propres au classique et des sons plus rocks au clavier notamment. Quoi qu'il en soit, le résultat est une belle envolée lyrique où l'on tutoie, au-delà les nuages, la stratosphère. Un cran au dessus. On en revient aux montagnes, à ce piano enneigé, aux choeurs et jeu de saxophone lascifs.

---
Ressources

- Une brève description de la signification du morceau : https://fr.wikipedia.org/wiki/Fool%27s_Overture
- Le morceau intégral, en version audio : https://play.the-redunicorn.fr/Supertramp_-_Fool_s_Overture.m4a