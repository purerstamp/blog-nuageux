---
Title: "Plaisir d'écoute #1"
Description: Avec Sonia Kronlund, on a les pieds sur Terre et parfois la tête dans les nuages
Date: 23 February 2022
Author: purerstamp
Profile: https://cloud.the-redunicorn.fr/u/edouard
Img: "%assets_url%/les-pieds-sur-terre.jpg"
Template: post
---

[Les Pieds sur Terre](https://www.franceculture.fr/emissions/les-pieds-sur-terre) est une émission radio de France Culture, racontant des brèves de vies bien réelles, telles que nous les connaissons, cotôyons, vivons, tous les jours.

Les histoires les plus rudes sont souvent les plus marquantes, parfois dérangeantes, parfois emplies de détresse. Mais il y a aussi celles qui finissent bien, celles qui font rêver. Ce sont celles-ci qui m'ont donné envie de vous partager ce coup de coeur, bien que cela fasse quelques temps que j'écoute régulièrement "les histoires, les enquêtes, les reportages" (au premier épisode que vous écouterez, vous aurez rapidement la référence ;p).

Aujourd'hui, deux histoires touchantes et sincères. La sincérité d'un petit garçon de six ans, avec ses mots et sa grammaire encore hésitante. Puis la sincérité d'un homme repris de justice, avec ses regrets et ses enseignements. Le petit Hans, tout d'abord, a pris le train comme un grand parce qu'il "s'ennuyait chez Mamie". Alors il est allé à la gare, est monté dans un de ces trains qu'il affectionne tant et s'est rendu à Lyon, direction le Parc de la Tête d'Or. Naturellement. Normalement. Bien évidemment, les cheminots l'ont pris en charge jusqu'à ce que son père vienne le chercher. Le témoignage du petit garçon rappelle bien des souvenirs. La naïveté infantine. L'insouciance des premières années. Une histoire réelle à travers laquelle, les pieds sur Terre, on ressent l'angoisse du père et pourtant, c'est la tête dans les nuages que l'on éprouve la nostalgie de l'enfance.

Point d'insouciance dans l'histoire suivante. Celle d'un homme qui voulait être un artiste et qui s'est tourné vers le grand banditisme. Pourtant, on sentirait presque une certaine candeur ou du moins, une insouciance. Celle d'un homme qui se retrouve sans emploi du jour au lendemain, qui met à profit son expérience dans la garde rapprochée pour devenir convoyeur de fonds puis simplement voleur de fonds. Puis la prison. Puis le désir de la perfection. Puis le braquage. Puis un côté décidément trop perfectionniste. Et enfin la cavale après un casse qui tourne mal. Un cercle vicieux où l'on se laisse entraîner sans vraiment s'en rendre compte, et de moins en moins à mesure que l'on se détache de la réalité. Finalement, après un ultime séjour en prison, l'occasion de faire le point, de rebondir, de reprendre les études et de sortir de cette spirale. Des regrets mais un parcours assumé, un équilibre qui laisse rêveur. Tout ne serait ni totalement noir ni totalement blanc.

---

Ressources

- Enregistrement de l'émission citée : https://www.franceculture.fr/emissions/les-pieds-sur-terre/petite-fugue-et-grande-cavales-deux-histoires-d-evasions
- Un article intéressant des Inrocks au sujet de la série : https://www.lesinrocks.com/actu/les-pieds-sur-terre-ou-letonnement-face-au-reel-141979-23-02-2019/