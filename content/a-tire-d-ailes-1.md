---
Title: "A tire d'ailes 1/3"
Description: "Envolez-vous avec cet hymne à plumes, inspiré par une création haute en couleurs de Elizabeth Hargrave. Partie 1."
Date: 23 March 2022
Author: purerstamp
Profile: https://cloud.the-redunicorn.fr/u/edouard
Img: "%assets_url%/wingspan.jpg"
Template: post
---

## Prologue

L'idée de cet article vient d'une découverte surprenante, dans le monde des jeux de plateau (ou *board games*). Surprenante parce que peu habitué de ce genre de jeux de société, je ne me doutais pas que cela existe. Cette découverte s'appelle *Wingspan, à tire d'ailes*. Il s'agit donc d'un jeu de plateau, mêlant stratégie, placement et surtout : oiseaux. Le principe du jeu est de placer des oiseaux dans différents habitats (forêt, plaine, marais) et d'accomplir des objectifs. Il faut gérer la nourriture, les oeufs ainsi que les pouvoirs des oiseaux qui sont des actions ou capacités généralement liées à la nature de l'oiseau ou à ses caractéristiques.

Et l'article dans tout ça ? Il suffit de regarder le jeu pour avoir le coup de coeur et saisir l'inspiration. En effet, le support principal sont les cartes oiseaux où sont dessinés les individus sous forme de splendides aquarelles. La boîte elle-même en est un bel exemple. Quand on joue, on découvre alors à mesure ces aquarelles ainsi que les commentaires et anecdotes sur l'espèce en question. On s'imagine l'oiseau, chantant, gazouillant, volant, planant, devant soi, majestueux, coloré, imposant, malin.

C'est de là que me vient l'inspiration et l'envie irrésistible de partager mon sentiment envers ce jeu et, plus largement, de parler d'oiseaux. Comme un hymne à ces animaux, dans leur symbolique, leurs mythes et leur omniprésence dans les arts et la culture.

## Mythologie et croyance populaire : un symbole multiple

L'une des première idées qui me soient venues à l'esprit au sujet des oiseaux, pour écrire cet article, est la présence de cet animal dans la culture populaire. Puis plus particulièrement, la symbolique autour de l'oiseau. Des oiseaux.

On pense alors au corbeau, à la colombe, à l'aigle, à la cigogne.. de multiples oiseaux en somme représentant pourtant chacun quelque chose de bien différent. Serait-ce leur capacité à fuir si facilement, d'un simple battement d'aile, qui aurait fasciné tant de peuples et de cultures ? Ou peut-être leur côté majestueux pour certains, ténébreux pour d'autres ? Ou encore un rêve si longtemps refusé à l'espèce humaine, celui de voler ?

La cigogne elle, perchée sur ses échasses, en impose lorsqu'elle claquette. Elle incarne a elle seule le contraste, celui de son plumage tantôt noir tantôt blanc, celui de son bec orangé, vif. Depuis tout temps, on est heureux de la voir revenir, elle annonce le Printemps, les beaux jours qui sont eux aussi de retour. Puis vient le moment où les cigogneaux arrivent à leur tour et trouvent en leurs parents un soutien sans faille, jusqu'à ce que leurs couleurs marquent enfin l'âge adulte. C'est d'ailleurs à ce titre que la cigogne symbolise la parentalité réussie et se charge d'amener nos bébés, selon certains.

Mais lorsque les jours sont moins heureux, moins lumineux, c'est bien souvent au corbeau que l'on pense. Invisible dans la nuit à la couleur de son plumage, son cri rauque le trahit. Il n'attend qu'une chose, sur un arbre perché, que l'aventureux trépasse afin d'en déguster la témérité fatale. On le dit alors de mauvaise augure, annonciateur de mort et de malheur. Pourtant, à l'autre bout du monde, on raconte que c'est le corbeau qui aurait apporté la lumière dans le monde. Il est là-bas vénéré comme le dieu solaire. Il suffit d'apprendre à le connaître.

A contrario, l'oiseau immaculé qu'est la colombe inspire généralement la pureté et la paix. Elle est le symbole de l'amour chez les amérindiens et le symbole de paix chez les judéochrétiens. Élevant son rameau d'olivier vers le ciel, elle nous reflète si parfaitement la lumière qu'elle apparaît d'une blancheur à faire rougir l'eau écarlate. 

L'aigle et le faucon sont également le symbole de l'élévation, celui de l'esprit. Considérés comme des êtres spirituels, leur vol si haut et si stable impose une grande sagesse. Qu'il soit égyptien ou inca, le faucon amène le soleil tous les jours pour en éclairer les peuples et les esprits. On envie cette capacité à admirer le monde dans son ensemble, tutoyant presque les étoiles, prenant le temps de la réflexion, de courant en courant. Ce sont des oiseaux qui se laissent porter, d'horizon en horizon, et dont la banalité des idées terre à terre ne les atteint pas.

Pratiquant lui aussi le vol à voile mais élevé à un rang inégalé, le condor expose fièrement son envergure exceptionnelle. Sa grandeur et la hauteur de son vol le rendent divin. Lui seul semble pouvoir accéder aux confins de l'horizon, là où les dieux peuvent y être rencontrés. Mais peut-être est-ce ce qui le rend effrayant, semblant pouvoir enlever un enfant ou un jeune agneau tout aussi innocent, entre ses serres acérées. Mais réhabilité à juste titre, il en devient un symbole d'amitié entre les peuples, celui de la réconciliation. Le condor aussi, il suffit d'apprendre à le connaître. Et *el condor pasa*.

---

Ressources

- La Bible des Signes & Symboles, Madonna Gauding, 2009
- Wikipédia : [Corbeau dans la culture](https://fr.wikipedia.org/wiki/Corbeau_dans_la_culture), [Le condor des Andes et l'homme](https://fr.wikipedia.org/wiki/Condor_des_Andes#Le_condor_des_Andes_et_l'homme)
- Crédits photo : Kim Euker et Stonemaier Games (en-tête)